#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

int len(char* string){
    int length = 0, i = 0;
    while(string[i++] != '\0'){
        ++length;
    }
    return length;
}

int main(int argc, char** argv){
    int index = 1;
    srand(time(NULL));
    if(argc > 1){
        while(index < argc){
            if(strcmp("-h", argv[index]) == 0 || strcmp("--help", argv[index]) == 0){
                printf("CLIRAND - a simple random picking tool. Without any flags, flips a coin once.\n");
                printf("USAGE: clirand [FLAGS] [ARGUMENTS]\n");
                printf("Available flags:\n");
                printf("    -d(--dice) [NUMBER] [NUMBER2] - rolls a NUMBER-sided dice NUMBER2 times. Rolls a six-sided dice by default.\n");
                printf("    -f(--flip) [NUMBER] - flips a coin NUMBER times, returns the heads/tails count. Flips a coin one time by default.\n");
                printf("    -t(--teams) [NUMBER] [ITEMS] - splits given ITEMS into NUMBER teams. Splits into two teams by default.\n");
                printf("    -g(--group) [NUMBER] [ITEMS] - splits given ITEMS into groups of NUMBER. If it's impossible to do evenly, splits randomly into groups not bigger than NUMBER. Splits into group of two by default.\n");
                printf("    -c(--choose) [NUMBER] [ITEMS] - chooses NUMBER items from ITEMS. Chooses one item by default.\n");
                return 0;
            }
            else if(strcmp("-d", argv[index]) == 0 || strcmp("--dice", argv[index]) == 0){
                if(++index < argc){
                    int sides = strtol(argv[index], NULL, 10);
                    if(++index < argc){
                        int times = strtol(argv[index], NULL, 10);
                        if(times == 0){
                            printf("Rolled a %d-sided dice. Result: %d\n", sides, rand()%sides+1);
                            continue;
                        }
                        int occurences[sides];
                        for(int i = 0; i < sides; ++i){
                            occurences[i] = 0;
                        }
                        for(int i = 0; i < times; ++i){
                            ++occurences[rand()%sides];
                        }
                        printf("Rolled a %d-sided dice %d times. Results:\n", sides, times);
                        for(int i = 0; i < sides; ++i){
                            if(occurences[i] > 0){
                                printf("%d: %d times\n", i + 1, occurences[i]);
                            }
                        }
                    }
                    else{
                        printf("Rolled a %d-sided dice. Result: %d\n", sides, rand()%sides+1);
                    }
                }
                else{
                    printf("Rolled a 6-sided dice. Result: %d\n", rand()%6+1);
                }
                continue;
            }
            else if(strcmp("-f", argv[index]) == 0 || strcmp("--flip", argv[index]) == 0){
                if(++index < argc){
                    int times = strtol(argv[index], NULL, 10);
                    if(times == 0){
                        printf("Flipped a coin. Result: ");
                        if(rand()%2 == 0)
                            printf("Heads.\n");
                        else 
                            printf("Tails.\n");
                        continue;
                    }
                    int occurences[2];
                    for(int i = 0; i < 2; ++i){
                        occurences[i] = 0;
                    }
                    for(int i = 0; i < times; ++i){
                        ++occurences[rand()%2];
                    }
                    printf("Flipped a coin %d times. Results:\n", times);
                    printf("Heads: %d times\n", occurences[0]);
                    printf("Tails: %d times\n", occurences[1]);
                }
                else{
                    printf("Flipped a coin. Result: ");
                    if(rand()%2 == 0)
                        printf("Heads.\n");
                    else 
                        printf("Tails.\n");
                }
                continue;
            }
            else if(strcmp("-t", argv[index]) == 0 || strcmp("--teams", argv[index]) == 0){
                int teams = strtol(argv[++index], NULL, 10), current_items = 0, team_occurences[teams], choice;
                if(teams < 0){
                    printf("Incorrect number of teams given.\n");
                    break;
                }
                if(teams == 0){
                    teams = 2;
                    --index;
                }
                if(teams > 0){
                    char **items = (char**)malloc(sizeof(char*) * 0);
                    while(++index < argc){
                        items = (char**)realloc(items, ++current_items*sizeof(char*));
                        items[current_items - 1] = (char*)malloc(sizeof(char)*1024);
                        strcpy(items[current_items - 1], argv[index]);
                    }
                    int item_teams[current_items];
                    for(int i = 0; i < teams; ++i){
                        team_occurences[i] = 0;
                    }
                    for(int i = 0; i < current_items; ++i){
                        choice = rand()%teams;
                        while(team_occurences[choice] >= ceil(current_items*1.0 / teams)){
                            choice = rand()%teams;
                        }
                        item_teams[i] = choice;
                        ++team_occurences[choice];
                    }
                    printf("Chosen teams:\n");
                    for(int i = 0; i < teams; ++i){
                        printf("  Team %d:\n", i + 1);
                        for(int j = 0; j < current_items; ++j){
                            if(item_teams[j] == i){
                                printf("    * %s\n", items[j]);
                            }
                        }
                    }
                    for(int i = 0; i < current_items; ++i){
                        free(items[i]);
                    }
                    free(items);
                }
                continue;
            }
            else if (strcmp("-g", argv[index]) == 0 || strcmp("--group", argv[index]) == 0){
                int members = strtol(argv[++index], NULL, 10), current_items = 0, choice;
                if(members < 0){
                    printf("Incorrect number of teams given.\n");
                    break;
                }
                if(members == 0){
                    members = 2;
                    --index;
                }
                if(members > 0){
                    char **items = (char**)malloc(sizeof(char*) * 0);
                    while(++index < argc){
                        items = (char**)realloc(items, ++current_items*sizeof(char*));
                        items[current_items - 1] = (char*)malloc(sizeof(char)*1024);
                        strcpy(items[current_items - 1], argv[index]);
                    }
                    int item_teams[current_items];
                    int group_count = ceil(current_items*1.0 / members), current_member_count[group_count];
                    for(int i = 0; i < group_count; ++i){
                        current_member_count[i] = 0;
                    }
                    for(int i = 0; i < current_items; ++i){
                        choice = rand()%group_count;
                        while(current_member_count[choice] >= members){
                            choice = rand()%group_count;
                        }
                        item_teams[i] = choice;
                        ++current_member_count[choice];
                    }
                    printf("Chosen groups:\n");
                    for(int i = 0; i < ceil(current_items*1.0 / members); ++i){
                        printf("  Group %d:\n", i + 1);
                        for(int j = 0; j < current_items; ++j){
                            if(item_teams[j] == i){
                                printf("    * %s\n", items[j]);
                            }
                        }
                    }
                    for(int i = 0; i < current_items; ++i){
                        free(items[i]);
                    }
                    free(items);
                }
                continue;
            }
            else{
                printf("Unknown option passed.\n");
            }
            ++index;
        }
    }
    else{
        if(rand()%2 == 0){
            printf("Heads.\n");
        }
        else{
            printf("Tails.\n");
        }
    }
    return 0;
}